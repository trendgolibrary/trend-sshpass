module gitlab.com/trendgolibrary/trend-sshpass

go 1.15

require (
	github.com/creack/pty v1.1.14
	github.com/riywo/loginshell v0.0.0-20200815045211-7d26008be1ab
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
