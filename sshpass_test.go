package sshpass

import (
	"github.com/sirupsen/logrus"
	"testing"
)

var CTX = Init(Configuration{
	Usr:  "root",
	Pwd:  "H0riz0n!",
	Host: "10.0.0.5:22",
})

func TestMkForwardProxyConfiguration(t *testing.T) {
	var res Response
	if err := CTX.MkForwardProxyConfiguration("06-salepage-knot.mazolic.com", &res).Error(); err != nil {
		logrus.Errorln("ControllerName, FunctionName SubFunctionName ▶", err)
		panic(err)
	}
	PrintStructJson(res)
}

func TestRmForwardProxyConfiguration(t *testing.T) {
	var res Response
	if err := CTX.RmForwardProxyConfiguration("06-salepage-knot.mazolic.com", &res).Error(); err != nil {
		logrus.Errorln("ControllerName, FunctionName SubFunctionName ▶", err)
		panic(err)
	}

	PrintStructJson(res)
}
