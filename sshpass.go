package sshpass

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Configuration struct {
	Usr  string
	Pwd  string
	Host string
}

type Response struct {
	DomainName  string  `json:"domain_name"`
	Success     bool    `json:"success"`
	MessageCode int     `json:"message_code"`
	MessageErr  *string `json:"message_err"`
}

type (
	Context interface {
		Error() error
		MkForwardProxyConfiguration(domainName string, response *Response) Context
		RmForwardProxyConfiguration(domainName string, response *Response) Context
	}
	context struct {
		conf Configuration
		err  error
	}
)

func (c *context) Error() error {
	if c.err != nil {
		return c.err
	}
	return nil
}

func (c context) MkForwardProxyConfiguration(domainName string, response *Response) Context {
	conn, err := Connect(c.conf.Host, c.conf.Usr, c.conf.Pwd)
	if err != nil {
		c.err = err
		return &c
	}
	cmdString := fmt.Sprintf("cd ~/script && bash nginx-stand.sh demo mk %s", domainName)
	output, err := conn.SendCommands(cmdString)
	if err != nil {
		c.err = err
		return &c
	}

	*response = checkResponseStatus(domainName, string(output))
	return &c
}

func (c context) RmForwardProxyConfiguration(domainName string, response *Response) Context {
	conn, err := Connect(c.conf.Host, c.conf.Usr, c.conf.Pwd)
	if err != nil {
		c.err = err
		return &c
	}
	cmdString := fmt.Sprintf("cd ~/script && bash nginx-stand.sh demo rm %s", domainName)

	output, err := conn.SendCommands(cmdString)
	if err != nil {
		c.err = err
		return &c
	}

	*response = checkResponseStatus(domainName, string(output))
	return &c
}

func Init(conf Configuration) Context {
	return &context{
		conf: conf,
	}
}

func CopyStructToStruct(target interface{}, result interface{}) error {
	fooByte, err := json.Marshal(&target)
	if err != nil {
		return err
	}

	err = json.Unmarshal(fooByte, result)
	if err != nil {
		return err
	}
	return nil
}

func PrintStructJson(target interface{}) {
	fooByte, _ := json.MarshalIndent(&target, "", "\t")
	fmt.Println(string(fooByte))
}

func checkResponseStatus(domainName string, response string) Response {
	if strings.Contains(response, "successful") {
		return Response{
			DomainName:  domainName,
			Success:     true,
			MessageCode: 200,
		}
	} else if strings.Contains(response, "already") {
		msg := `already exists`
		return Response{
			DomainName:  domainName,
			Success:     false,
			MessageCode: 409,
			MessageErr:  &msg,
		}
	} else if strings.Contains(response, "domain not found") {
		msg := `domain not found`
		return Response{
			DomainName:  domainName,
			Success:     false,
			MessageCode: 404,
			MessageErr:  &msg,
		}
	} else {
		return Response{
			DomainName:  domainName,
			Success:     false,
			MessageCode: 1002,
			MessageErr:  &response,
		}
	}
}
